const express = require('express');
const fs = require('fs');
const morgan = require('morgan');
const app = express();

app.use(morgan('tiny'));
app.use(express.json());

if( !fs.existsSync('./files') ) {
    fs.mkdirSync('./files');
}

app.get('/api/files', (req, res) => {
    fs.readdir('./files', (err, files) => {
        if( err ) {
            console.log(err);
            res.status(500).json({"message" : "Server error"})
        }
        const filesArray = [];
        files.forEach(file => filesArray.push(file));
        res.status(200).json(filesArray);
    })
})

// terrible quickfix 
const getFile = (filename, res) => {
    fs.readFile(`./files/${filename}`, (err, data) => {
        if( err ) {
            res.status(400).json({"message" : `No file with ${filename} filename found`});
        } else {
            const extensionRegexp = /\.{1}([\w]+)$/;
            res.status(200).json({
                "message" : "Success",
                "filename" : filename,
                "content" : data.toString('utf-8'),
                "extension" : filename.match(extensionRegexp)[1],
                "uploadedDate" : fs.statSync(`./files/${filename}`).birthtime
            });
        }
    })
}

app.get('/api/files/:filename', async (req, res) => {
    const filename = req.params.filename;
    const password = req.query.password;
    const protectedFiles = JSON.parse(fs.readFileSync('./passwordProtectedFiles.json', 'utf-8'));
    if( filename in protectedFiles ) {
        if( password !== protectedFiles[filename] ) {
            res.status(400).json({"message" : "Wrong password!"});
        } else {
            getFile(filename, res);
        }
    } else {
        getFile(filename, res);
    }
})

app.post('/api/files', (req, res) => {
    const { filename, content, password } = req.body;
    const allowedFileExtensions = /\.(txt|json|yaml|xml|js)+$/;

    if( filename === undefined ) {
        res.status(400).json({"message" : "Please specify 'filename' parameter"});
    } else if( !allowedFileExtensions.test(filename) ) {
        res.status(400).json({"message" : "The file extension is not supported!"});
    } else if( fs.existsSync(`./files/${filename}`) ) {
        res.status(400).json({"message" : `File ${filename} already exists!`});
    } else if( content === undefined ) {
        res.status(400).json({"message" : "Please specify 'content' parameter"});
    } else {
        if( password ) {
            fs.readFile('./passwordProtectedFiles.json', (err, data) => {
                if( err ) {
                    res.status(500).json({"message": "Server error"});
                }
                const stringData = JSON.parse(data.toString());
                stringData[filename] = String(password);
                fs.writeFile('./passwordProtectedFiles.json', JSON.stringify(stringData), (err) => {
                    if( err ) {
                        console.log(err);
                        res.status(500).json({"message" : "Server error"});
                    }
                })
            })
        }
        fs.writeFile(`./files/${filename}`, content, err => {
            if( err ) {
                console.log(err); 
                res.status(500).json({ "message" : "Server error" });
            } else {
                res.status(200).json({"message": "File created successfully"});
            }
        });
    }
})

app.delete('/api/files/:filename', (req, res) => {
    const filename = req.params.filename;
    fs.unlink(`./files/${filename}`, err => {
        if( err ) {
            console.log(err);
            res.status(500).json({"message" : "Server error"});
        } else {
            res.status(200).json({"message" : `file ${filename} was successfuly deleted!`});
        }
    })
})

app.put('/api/files/:filename', (req, res) => {
    const filename = req.params.filename;
    const content = req.body.content;

    if( !fs.existsSync(`./files/${filename}`) ) {
        res.status(400).json({"message" : "The file does not exist!"});
    }
    
    fs.writeFile(`./files/${filename}`, content, err => {
        if( err ) {
            console.log(err);
            res.status(500).json({"message" : "Server error"});
        } else {
            res.status(200).json({"message" : `File ${filename} was successfully updated!`});
        }
    })
})
app.listen(8080);
